package org.gitcoolstuff.proxy.service;

import com.google.common.collect.Lists;
import org.gitcoolstuff.proxy.entity.Ingredient;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientServiceImpl implements IngredientService {
    @Override
    public List<Ingredient> getAll() {
        return Lists.newArrayList();
    }
}
