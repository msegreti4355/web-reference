package org.gitcoolstuff.proxy.service;

import org.gitcoolstuff.proxy.entity.Ingredient;

import java.util.List;

public interface IngredientService {
    List<Ingredient> getAll();
}
