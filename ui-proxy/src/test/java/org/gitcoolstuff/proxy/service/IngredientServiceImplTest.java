package org.gitcoolstuff.proxy.service;

import org.gitcoolstuff.proxy.entity.Ingredient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { IngredientServiceImplTest.TestContext.class })
public class IngredientServiceImplTest {

    @Autowired
    IngredientService ingredientService;

    @Test
    public void getAllTest(){
        List<Ingredient> allIngredients =  ingredientService.getAll();
        assertThat(allIngredients, is(hasSize(0)));
    }

    @Configuration
    public static class TestContext {
        @Bean
        IngredientService ingedientService(){
            return new IngredientServiceImpl();
        }
    }
}
