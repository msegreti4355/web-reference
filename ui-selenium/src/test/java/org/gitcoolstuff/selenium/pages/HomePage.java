package org.gitcoolstuff.selenium.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebElement;

@DefaultUrl("http://localhost:9090/#/home")
public class HomePage extends PageObject {

    @FindBy(xpath = "html/body/div[1]/div/md-content/div/div/div[1]/button")
    private WebElement helloButton;

    @FindBy(xpath = "html/body/div[3]/md-dialog/md-dialog-content/h2")
    private WebElement alertDialogH2;

    public WebElement getHelloButton() {
        return helloButton;
    }

    public WebElement getAlertDialogH2() {
        return alertDialogH2;
    }
}
