package org.gitcoolstuff.selenium;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.bouncycastle.jcajce.provider.symmetric.ARC4;
import org.gitcoolstuff.selenium.pages.HomePage;
import org.gitcoolstuff.selenium.steps.HomeSteps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SerenityRunner.class)
public class HomeTests extends BaseTest {

    @Steps
    HomeSteps homeSteps;

    @Test
    public void testTheHelloThereButton() {
        homeSteps.openHomePage();
        homeSteps.clickHelloThereButton();
        String headerText = homeSteps.getTheAlertDialogTitle();
        assertThat(headerText, is(equalTo("This is an alert title")));
    }
}
