package org.gitcoolstuff.selenium.steps;


import net.thucydides.core.steps.ScenarioSteps;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseSteps extends ScenarioSteps {

    public void waitForAngular() {
        WebDriverWait wait = new WebDriverWait(getDriver(), 15, 100);
        wait.until(angularHasFinishedProcessing());
    }

    private ExpectedCondition<Boolean> angularHasFinishedProcessing() {
        return driver1 -> {
            String hasAngularFinishedScript = "var callback = arguments[arguments.length - 1];\n" +
                    "var el = document.querySelector('html');\n" +
                    "if (!window.angular) {\n" +
                    "    callback('false')\n" +
                    "}\n" +
                    "if (angular.getTestability) {\n" +
                    "    angular.getTestability(el).whenStable(function(){callback('true')});\n" +
                    "} else {\n" +
                    "    if (!angular.element(el).injector()) {\n" +
                    "        callback('false')\n" +
                    "    }\n" +
                    "    var browser = angular.element(el).injector().get('$browser');\n" +
                    "    browser.notifyWhenNoOutstandingRequests(function(){callback('true')});\n" +
                    "}";

            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver1;
            String isProcessingFinished = javascriptExecutor.executeAsyncScript(hasAngularFinishedScript).toString();

            return Boolean.valueOf(isProcessingFinished);
        };
    }
}
