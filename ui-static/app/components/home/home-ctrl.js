angular.module('App').controller('HomeCtrl', function ($scope, $mdDialog) {
    $scope.myDate = new Date();

    $scope.minDate = new Date($scope.myDate.getFullYear(),
        $scope.myDate.getMonth() - 2, $scope.myDate.getDate());

    $scope.maxDate = new Date($scope.myDate.getFullYear(),
        $scope.myDate.getMonth() + 2, $scope.myDate.getDate());

    $scope.onlyWeekendsPredicate = function (date) {
        var day = date.getDay();
        return day === 0 || day === 6;
    };

    $scope.showAlert = function (ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.body))
                .clickOutsideToClose(false)
                .title('This is an alert title')
                .textContent('You can specify some description text in here.')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };
});
