angular.module('App').controller('SexChangeCtrl', function SexChangeCtrl($scope, $mdDialog, currentName) {

	var steps = [{
	        id: 		'name',
	        title: 		'Enter the person\'s name',
	        next: 		'sex',
	        nextTitle: 	'Choose a sex'

	    }, {
	        id: 		'sex',
	        title: 		'Choose a sex',
	        previous: 	'name',
	        nextTitle: 	'Enter a name',

	        finishAction: function finishAction(event) {

				var textContent = ($scope.person.sex.toUpperCase() === 'M' ? 'Mr. ' : 'Ms. ') +
		    			$scope.person.fullname;

				var dialog = $mdDialog.alert()
		                .parent(angular.element(document.body))
		                .clickOutsideToClose(false)
		                .title('Presenting')
		                .textContent(textContent)
		                .ariaLabel('Presenting')
		                .targetEvent(event);

		        $mdDialog.show(dialog);
			}
		}];

	$scope.next = function next() {
        if ($scope.currentStep && $scope.hasNext($scope.currentStep)) {
            $scope.currentStep = steps[$scope.currentStep.next];
        }
	};

    $scope.previous = function previous() {
        if ($scope.currentStep && $scope.hasPrevious($scope.currentStep)) {
            $scope.currentStep = steps[$scope.currentStep.previous];
        }
    };

    $scope.hasNext = function hasNext(step) {
        return _.has(step, 'next');
    };

    $scope.hasPrevious = function hasPrevious(step) {
        return _.has(step, 'previous');
    };

	$scope.init = function init() {

		$scope.sexes = [
			{ id: 'M', description: 'Male' },
			{ id: 'F', description: 'Female' },
			{ id: 'U', description: 'Unknown' }
		];

		$scope.currentStep = steps[0];

		$scope.person = {
			fullname: currentName || undefined
		};
	};

	$scope.init();
});