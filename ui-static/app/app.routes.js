angular.module('App').config(function ($routeProvider) {

  $routeProvider.when('/home', {templateUrl: 'app/components/home/home-view.html'});

  /* Add New Routes Above */
  $routeProvider.otherwise({redirectTo: '/home'});

});
