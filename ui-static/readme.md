# ui-static
##Setup
- Install the latest version of Node JS https://nodejs.org/en/
- execute 'npm install -g bower'
- execute 'npm install -g grunt-cli'
- execute 'npm install'
- execute 'bower install'

####To "build" the UI
- execute 'grunt build'

####To "Serve" the UI
- execute 'grunt serve'
 The UI should be accessible at http://localhost:9090

## Standards
* [Directory Structure](https://scotch.io/tutorials/angularjs-best-practices-directory-structure)
* [Naming conventions](https://github.com/mgechev/angularjs-style-guide#naming-convensions)
* [Controllers](https://github.com/mgechev/angularjs-style-guide#controllers)
* [Directives](https://github.com/mgechev/angularjs-style-guide#directives)
* [Icons](https://www.google.com/design/icons/)
